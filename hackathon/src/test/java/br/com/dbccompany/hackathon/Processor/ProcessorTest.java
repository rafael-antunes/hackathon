package br.com.dbccompany.hackathon.Processor;

import br.com.dbccompany.hackathon.Entity.Dados;
import br.com.dbccompany.hackathon.Entity.DadosWriter;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ProcessorTest {

    @Test
    void processRetornaNumeroDeVendedores() throws Exception {
        Processor processor = new Processor();
        DadosWriter dadosWriter;
        dadosWriter = processor.process(new Dados( "001", "1234567981324", "Pedro", "5000" ));
        dadosWriter = processor.process(new Dados( "001", "3245678865434", "Paulo", "4000.99" ));
        dadosWriter.equals(new DadosWriter("0", "2", null, null));
    }

    @Test
    void processRetornaNumeroDeClientes() throws Exception {
        Processor processor = new Processor();
        DadosWriter dadosWriter;
        dadosWriter = processor.process(new Dados("002", "2345675434544345", "JoseDaSilva", "Rural"));
        dadosWriter = processor.process(new Dados("002", "2345675433444345", "EduardoPereira", "Rural"));
        dadosWriter = processor.process(new Dados("002", "2345675433444545", "JosePereira", "Rural"));
        dadosWriter.equals(new DadosWriter("3","0",null,null));
    }

    @Test
    void processRetornaIdMaiorVenda () throws Exception {
        Processor processor = new Processor();
        DadosWriter dadosWriter;
        dadosWriter = processor.process(new Dados("003", "10", "[1-10-100,2-30-2.50,3-40-3.10]", "Pedro"));
        dadosWriter = processor.process(new Dados("003", "08", "[1-34-10,2-33-1.50,3-40-0.10]", "Paulo"));
        dadosWriter.equals(new DadosWriter("0", "0", "10", null));
    }

    @Test
    void processRetornaPiorVendedor () throws Exception {
        Processor processor = new Processor();
        DadosWriter dadosWriter;
        dadosWriter = processor.process(new Dados( "001", "1234567981324", "Pedro", "5000" ));
        dadosWriter = processor.process(new Dados( "001", "3245678865434", "Paulo", "4000.99" ));
        dadosWriter = processor.process(new Dados("003", "10", "[1-10-100,2-30-2.50,3-40-3.10]", "Pedro"));
        dadosWriter = processor.process(new Dados("003", "08", "[1-34-10,2-33-1.50,3-40-0.10]", "Paulo"));
        dadosWriter.equals(new DadosWriter("0", "2", "10", "Paulo"));
    }

    @Test
    void processRetornaTodasInformacoesCorretasDoInputDeDadosDeReferencia () throws Exception {
        Processor processor = new Processor();
        DadosWriter dadosWriter;
        dadosWriter = processor.process(new Dados( "001", "1234567981324", "Pedro", "5000" ));
        dadosWriter = processor.process(new Dados( "001", "3245678865434", "Paulo", "4000.99" ));
        dadosWriter = processor.process(new Dados("002", "2345675434544345", "JoseDaSilva", "Rural"));
        dadosWriter = processor.process(new Dados("002", "2345675433444345", "EduardoPereira", "Rural"));
        dadosWriter = processor.process(new Dados("003", "10", "[1-10-100,2-30-2.50,3-40-3.10]", "Pedro"));
        dadosWriter = processor.process(new Dados("003", "08", "[1-34-10,2-33-1.50,3-40-0.10]", "Paulo"));
        dadosWriter.equals(new DadosWriter("2", "2", "10", "Paulo"));
    }
}
