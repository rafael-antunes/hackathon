package br.com.dbccompany.hackathon.Writer;

import br.com.dbccompany.hackathon.Entity.DadosWriter;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class Writer{

    private final Resource outputResource = new FileSystemResource("data/out/" + LocalDateTime.now() + ".done.dat");

    @Bean
    public FlatFileItemWriter<DadosWriter> writerDaClasse()
    {
        FlatFileItemWriter<DadosWriter> writer = new FlatFileItemWriter<>();

        writer.setResource(outputResource);

        writer.setAppendAllowed(false);

        writer.setLineAggregator(new DelimitedLineAggregator<DadosWriter>() {
            {
                setDelimiter(" ");
                setFieldExtractor(new BeanWrapperFieldExtractor<DadosWriter>() {
                    {
                        setNames(new String[] { "quantidadeDeClientes", "quantidadeDeVendedores", "idVendaMaisCara", "piorVendedor" });
                    }
                });
            }
        });
        return writer;
    }

}
