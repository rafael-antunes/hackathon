package br.com.dbccompany.hackathon.Entity;

public class Dados {

    private String id;
    private String primeiraInfo;
    private String segundaInfo;
    private String terceiraInfo;

    public Dados(String id, String primeiraInfo, String segundaInfo, String terceiraInfo) {
        this.id = id;
        this.primeiraInfo = primeiraInfo;
        this.segundaInfo = segundaInfo;
        this.terceiraInfo = terceiraInfo;
    }

    public Dados() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrimeiraInfo() {
        return primeiraInfo;
    }

    public void setPrimeiraInfo(String primeiraInfo) {
        this.primeiraInfo = primeiraInfo;
    }

    public String getSegundaInfo() {
        return segundaInfo;
    }

    public void setSegundaInfo(String segundaInfo) {
        this.segundaInfo = segundaInfo;
    }

    public String getTerceiraInfo() {
        return terceiraInfo;
    }

    public void setTerceiraInfo(String terceiraInfo) {
        this.terceiraInfo = terceiraInfo;
    }

}
