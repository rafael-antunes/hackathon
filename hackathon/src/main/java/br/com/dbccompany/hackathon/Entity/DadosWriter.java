package br.com.dbccompany.hackathon.Entity;

public class DadosWriter {

    private String quantidadeDeClientes;
    private String QuantidadeDeVendedores;
    private String idVendaMaisCara;
    private String piorVendedor;

    public DadosWriter(String quantidadeDeClientes, String quantidadeDeVendedores, String idVendaMaisCara, String piorVendedor) {
        this.quantidadeDeClientes = quantidadeDeClientes;
        QuantidadeDeVendedores = quantidadeDeVendedores;
        this.idVendaMaisCara = idVendaMaisCara;
        this.piorVendedor = piorVendedor;
    }

    public DadosWriter() {
    }

    public String getQuantidadeDeClientes() {
        return quantidadeDeClientes;
    }

    public void setQuantidadeDeClientes(String quantidadeDeClientes) {
        this.quantidadeDeClientes = quantidadeDeClientes;
    }

    public String getQuantidadeDeVendedores() {
        return QuantidadeDeVendedores;
    }

    public void setQuantidadeDeVendedores(String quantidadeDeVendedores) {
        QuantidadeDeVendedores = quantidadeDeVendedores;
    }

    public String getIdVendaMaisCara() {
        return idVendaMaisCara;
    }

    public void setIdVendaMaisCara(String idVendaMaisCara) {
        this.idVendaMaisCara = idVendaMaisCara;
    }

    public String getPiorVendedor() {
        return piorVendedor;
    }

    public void setPiorVendedor(String piorVendedor) {
        this.piorVendedor = piorVendedor;
    }
}
